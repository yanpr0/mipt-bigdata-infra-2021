# SQL-basic
**Дедлайн: 23 мая 23:59**

На семинаре было рассказано о том, что такое flex и bison, а также как
с помощью них задавать КС-грамматики.
Реализуем SQL-интерфейс, который уже поддерживает клиент, но не поддерживает
сервер.

Рекомендуется начать с `/lib/server/sql`. В данной директории практически все написано за вас, но
стоит обратить внимание:
* КС-грамматика должная поддерживать UTF-8 символы в экранированных строках, например, match("купить телефон").
Эту часть, а также другие пропуски нужно заполнить в `scanner.l`
* AST-дерево содержит реализацию базовых вершин, но не все, дописать их нужно в `ast.h/ast.cpp`
* Затем остается прописать правила грамматики в `parser.y`

Когда все готово осталось вернуться `/lib/server/net` и заменить заглушку на обработку SQL-запроса.
Воспользуйтесь вспомогательным классом Driver из `/lib/server/sql/driver.hh`

Задание будет проверяться вручную. Критерием успеха данной задачи является успешное выполнение и обработка запросов
вида `SELECT * FROM <index_name> WHERE match("<sentence>")`